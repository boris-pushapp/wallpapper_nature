package thanjava.more.kotleng.wallppr_outdoors

import android.Manifest
import android.app.WallpaperManager
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_main.*
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.gson.internal.LinkedTreeMap
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var fadeIn = AlphaAnimation(0f, 1f)
    private var fadeOut = AlphaAnimation(1f, 0f)
    private var btnVisible = true
    private val imgs = ArrayList<String>()
    private lateinit var adapter: SimpleAdapter
    private val api_key = "b39fb08adba29563cc391156b7073c3c7fae4c9333093578e23879be204a4315"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        getWindow().setStatusBarColor(resources.getColor(R.color.qweqwe))
        adapter = SimpleAdapter(supportFragmentManager)
        vwpgr.adapter = adapter
        initUrls()
        attachClicks()
    }

    inner class SimpleAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
        override fun getItem(position: Int): Fragment = ImageFragment.newInstance(imgs[position])
        override fun getCount(): Int = imgs.size
    }

    private fun attachClicks() {
        btn_dwnld.setOnClickListener(this)
        btn_wllppr.setOnClickListener(this)
        vwpgr.setOnClickListener(this)
        cnntr.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            btn_dwnld.id -> downloadImg()
            btn_wllppr.id -> setWllppr()
        }
    }

    fun showBtns() {
        btnVisible=!btnVisible
        Log.d("Visible", btnVisible.toString())
        if (btnVisible) {
            vwpgr.isEnabled = false
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            btmbr.visibility = View.VISIBLE
            btmbr.animate().setDuration(500).setInterpolator(AccelerateInterpolator()).alpha(1f).withEndAction({
                vwpgr.isEnabled = true
                btnVisible = true
            })
        } else {
            vwpgr.isEnabled = false
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

            btmbr.animate().setDuration(500).setInterpolator(AccelerateInterpolator()).alpha(0f).withEndAction({
                btmbr.visibility = View.GONE
                vwpgr.isEnabled = true
                btnVisible = false
            })
        }
    }


    private fun setWllppr() {

        val size = windowManager.defaultDisplay
        Log.d("ScreenSize", "height: " + size.height + " width: " + size.width)
        Picasso.with(this).load(imgs[vwpgr.currentItem] + "&fit=crop&w="+size.width+"&h="+size.height).into(object : Target {
            override fun onBitmapFailed(errorDrawable: Drawable?) {
                Log.d("kek", "failur")

            }

            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                val myWallpaperManager = WallpaperManager.getInstance(applicationContext)
                try {
/*
                        val diff = size.height/size.width
                        val height = bitmap.height/diff
                        val b = Bitmap.createBitmap(bitmap, )*/
                    myWallpaperManager.setBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                Log.d("kek", "prepare")
            }

        })
    }

    private fun downloadImg() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    50)
        }
        val size = windowManager.defaultDisplay
        Log.d("ScreenSize", "height: " + size.height + " width: " + size.width)
        Picasso.with(this).load(imgs[vwpgr.currentItem] + "&fit=crop&w="+size.width+"&h="+size.height).into(object : Target {
            override fun onBitmapFailed(errorDrawable: Drawable?) {
                Log.d("kek", "failur")

            }

            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                Thread(Runnable {
                    var file = File(Environment.getExternalStorageDirectory(), getString(R.string.app_name))
                    if (!file.exists()) {
                        file.mkdirs()
                    }
                    file = File(Environment.getExternalStorageDirectory().getPath() + "/" + getString(R.string.app_name) + "/image" + vwpgr.currentItem + ".jpg")
                    try {
                        file.createNewFile()
                        val ostream = FileOutputStream(file)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream)
                        ostream.flush()
                        ostream.close()
                        Log.d("succes", imgs[vwpgr.currentItem])
                        runOnUiThread {
                            Toast.makeText(this@MainActivity, "Файл загружен", Toast.LENGTH_LONG).show()

                        }

                    } catch (e: IOException) {
                        Log.d("IOException", e.getLocalizedMessage())
                    }
                }).start()

            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                Log.d("kek", "prepare")
            }
        })
    }


    fun initUrls() {
        ImgsApi.create().getImgList(api_key, "100").enqueue(object : Callback<List<HashMap<String, Any>>> {
            override fun onFailure(call: Call<List<HashMap<String, Any>>>?, t: Throwable?) {
                Log.d("fail", "fail")
            }

            override fun onResponse(call: Call<List<HashMap<String, Any>>>?, response: Response<List<HashMap<String, Any>>>) {
                val objects = ArrayList<HashMap<String, Any>>(response.body())
                Log.d("kek", objects.toString())
                for (obj in objects)
                    imgs.add((obj["urls"] as LinkedTreeMap<String, String>)["full"].toString())

                adapter.notifyDataSetChanged()
                //images.add(obj.urls.regular + "/1080x1920")
                //Log.d("id", (obj["urls"] as LinkedTreeMap<String, String>)["regular"].toString())

                //adapter.notifyDataSetChanged()
            }
        })
    }
}
