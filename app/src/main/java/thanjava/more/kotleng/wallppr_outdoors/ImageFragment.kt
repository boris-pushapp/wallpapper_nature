package thanjava.more.kotleng.wallppr_outdoors


import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_image.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val URL = "param1"

/**
 * A simple [Fragment] subclass.
 * Use the [ImageFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ImageFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(URL)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_image, container, false)
        view.pb.visibility = View.VISIBLE
        view.img.visibility = View.GONE
        val size = (context as Activity).windowManager.defaultDisplay
        view.setOnClickListener({(context as MainActivity).showBtns()})
        //Picasso.with(context).load(arguments!!.getString(URL)+"&fit=crop&max-w=2048&max-h=2048").skipMemoryCache().into(view.img, object : Callback{
        Picasso.with(context).load(arguments!!.getString(URL)+"&fit=crop&w="+size.width+"&h="+size.height).skipMemoryCache().into(view.img, object : Callback{
            override fun onSuccess() {
                view.pb.visibility = View.GONE
                view.img.visibility = View.VISIBLE
            }

            override fun onError() {
            }
        })
        return view
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ImageFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String) =
                ImageFragment().apply {
                    arguments = Bundle().apply {
                        putString(URL, param1)
                    }
                }
    }
}
