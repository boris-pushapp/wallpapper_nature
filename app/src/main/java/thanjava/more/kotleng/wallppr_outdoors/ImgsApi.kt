package thanjava.more.kotleng.wallppr_outdoors

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ImgsApi {
    @GET("photos")
    fun getImgList(@Query("client_id") str: String, @Query("per_page") size: String): Call<List<HashMap<String, Any>>>

    companion object {
        fun create(): ImgsApi {
            val builder = Retrofit.Builder().baseUrl("https://api.unsplash.com/collections/289662/").addConverterFactory(GsonConverterFactory.create()).build()
            return builder.create(ImgsApi::class.java)
        }
    }

}